#ifndef __T_MATRIX_H___
#define __T_MATRIX_H___

#include <assert.h>
#include <iostream>
#include <vector>


//YOUR CODE GOES HERE!



// Convert these declarations to definitions (i.e., implement)
template<typename T> std::ostream & operator<<(std::ostream & s, const vector<T> & rhs);
template<typename T> std::ostream & operator<<(std::ostream & s, const Matrix<T> & rhs);

#endif
